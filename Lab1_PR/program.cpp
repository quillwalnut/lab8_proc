#include "stdafx.h"
#include "program.h"
#include <string>
#include <fstream>
#include <iostream>

using namespace std;

namespace simple_shapes
{
	struct aphorism;
	struct proverb;
	struct riddle;
	struct shape;
	struct Node;
	struct List;

	void Init(struct List &lst)
	{
		lst.Head = NULL;
		lst.Tail = NULL;
		lst.size = 0;
	}

	void Clear(struct List &lst)
	{
		while (lst.size != 0) //���� ����������� ������ �� ������ ������� 
		{
			Node *temp = lst.Head->Next;
			delete lst.Head; //����������� ������ �� ��������� ��������
			lst.Head = temp; //����� ������ ������ �� ����� ���������� ��������
			lst.size--; //���� ������� ����������. ������������ ����� ���������
		}
	}

	void In(struct List &lst, ifstream &ifst)
	{
		while (!ifst.eof())
		{
			lst.size++; //��� ������ ���������� �������� ����������� ����� ��������� � ������
			Node  *temp = new Node; //��������� ������ ��� ������ �������� ������
			temp->Next = lst.Head; //��������� �������. ��������� ������� - ��� ������ ������ 
			temp->x = In(ifst); //���������� � ���������� ������ ������ �������� x 

			if (lst.Head != NULL) //� ��� ������ ���� ������ �� ������
			{
				lst.Tail->Next = temp; //������ ������ � ��������� �� ��������� ��������� ����
				lst.Tail = temp; //��������� �������� �������=������ ��� ���������.
			}
			else
			{
				lst.Head = lst.Tail = temp;//���� ������ ���� �� ��������� ������ �������.
			}
		}
	}

	void Out(struct List &lst, ofstream &ofst)
	{
		ofst << "Container contains " << lst.size
			<< " elements." << endl;
		Node *tempHead = lst.Head; //��������� �� ������

		int temp = lst.size; //��������� ���������� ������ ����� ��������� � ������
		int i = 0;
		while (temp != 0) //���� �� �������� ������� ������� �� ����� ������
		{
			ofst << i << ": ";
			i++;
			Out(*(tempHead->x), ofst); //��������� ������� ������ �� ����� 
			tempHead = tempHead->Next; //���������, ��� ����� ��������� �������
			temp--; //���� ������� ������, ������ �������� �� ���� ������ 
		}
	}

	void In(aphorism &a, ifstream &ifst)
	{		
		ifst.getline(a.author, 256, '\n');
	}

	void In(proverb &p, ifstream &ifst)
	{		
		ifst.getline(p.country, 256, '\n');
	}

	void In(riddle &r, ifstream &ifst)
	{
		ifst.getline(r.answer, 256, '\n');
	}

	shape* In(ifstream &ifst)
	{
		shape *sp;
		int k;
		ifst >> k;
		char temp[256];
		ifst.getline(temp, 256, '\n');
		switch (k) 
		{
		case 1:
			sp = new shape;
			sp->k = shape::key::APHORISM;
			In(sp->a, ifst);
			break;
		case 2:
			sp = new shape;
			sp->k = shape::key::PROVERB;
			In(sp->p, ifst);
			break;
		case 3:
			sp = new shape;
			sp->k = shape::key::RIDDLE;
			In(sp->r, ifst);
			break;
		default:
			return 0;
		}
		ifst.getline(sp->text, 256, '\n');
		ifst >> sp->assessment;
		return sp;
	}

	void Out(shape &s, ofstream &ofst) {
		switch (s.k) {
		case shape::key::APHORISM:
			Out(s.a, ofst);
			break;
		case shape::key::PROVERB:
			Out(s.p, ofst);
			break;
		case shape::key::RIDDLE:
			Out(s.r, ofst);
			break;
		default:
			ofst << "Incorrect figure!" << endl;
			return;
		}
		ofst << ", text = " << s.text << ", assessment = " << s.assessment
			<< ", count of punctuation marks = " << Count(s) << endl;
	}

	void Out(aphorism &a, ofstream &ofst)
	{
		ofst << "It is Aphorism: author = "
			<< a.author;
	}

	void Out(proverb &p, ofstream &ofst)
	{
		ofst << "It is Proverb: country = "
			<< p.country;

	}

	void Out(riddle &r, ofstream &ofst)
	{
		ofst << "It is Riddle: answer = "
			<< r.answer;
	}

	int Count(shape &s)
	{
		int result = 0;
		for (int i = 0; i < strlen(s.text); i++)
		if (s.text[i] == ',' || s.text[i] == '.' || s.text[i] == '?' ||
			s.text[i] == '!' || s.text[i] == '(' || s.text[i] == ')' ||
			s.text[i] == '"' || s.text[i] == '-' || s.text[i] == ':')
			result++;
		return result;
	}

	bool Compare(shape *first, shape *second)
	{
		return Count(*first) < Count(*second);		
	}

	void Sort(struct List &lst)
	{
		Node *p = lst.Head;
		for (int i = 0; i < lst.size - 1; i++)
		{
			Node *temp = p->Next;
			for (int j = i + 1; j < lst.size; j++)
			{
				if (Compare(p->x, temp->x))
				{
					shape *tmp = p->x;
					p->x = temp->x;
					temp->x = tmp;
				}
				temp = temp->Next;
			}
			p = p->Next;
		}
	}

	void OutAphorisms(struct List &lst, ofstream &ofst)
	{
		ofst << "Only Aphorisms." << endl;
		Node *tempHead = lst.Head; //��������� �� ������

		int temp = lst.size; //��������� ���������� ������ ����� ��������� � ������
		int i = 0;
		while (temp != 0) //���� �� �������� ������� ������� �� ����� ������
		{
			ofst << i << ": ";
			i++;
			if (tempHead->x->k == shape::APHORISM)
				Out(*(tempHead->x), ofst); //��������� ������� ������ �� ����� 
			else ofst << endl;
			tempHead = tempHead->Next; //���������, ��� ����� ��������� �������
			temp--; //���� ������� ������, ������ �������� �� ���� ������ 
		}
	}
}